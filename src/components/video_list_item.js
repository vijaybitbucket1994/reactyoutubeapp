import React from "react";

const VideoListItem = ({ video, onVideoSelect }) => {
  const ImageUrl = video.thumbnails.default.url;
  const Title = video.title;

  return (
    <li onClick={() => onVideoSelect(video)} className="list-group-item">
      <div className="video-list media">
        <div className="media-left">
          <img src={ImageUrl} className="media-object" />
        </div>

        <div className="media-body">
          <h5 className="media-title">{Title}</h5>
        </div>
      </div>
    </li>
  );
};

export default VideoListItem;
