import React from "react";

const VideoDetail=({video})=> {

    if(!video){

        return (<div className="text-warning">Loading</div>)
    }

    const VideoId=video.id;
    const VideoUrl=`https://www.youtube.com/embed/${VideoId}`;

    return (
        <div className="video-detail col-md-7 mx-auto">
            <div className="embed-responsive embed-responsive-16by9">
                <iframe src={VideoUrl}></iframe>  
            </div>

            <div className="detail">
                 <h5>
                     {video.title}
                </h5>
                    <p>
                      {video.description}
                     </p>
            </div>
        </div>

    )
}

export default VideoDetail;
