import React,  {Component}  from "react";

class SearchBar extends React.Component{

    constructor(props){
        super(props);
        this.state={
            term:""
        }; 
    }

    render(){
        return( 
            <div className="col-md-7 mx-auto my-3 search-bar">
            <input onChange={(event)=>this.onInputChange(event.target.value)} className="input-lg" value={this.state.term}  /> 
            </div>    
    )
    }

    onInputChange(term){
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;