import React, { Component } from "react";
import ReactDOM from "react-dom";
import SearchBar from "./components/search_bar";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";
import _ from "lodash";
import style from "./style/style.css"



const search = require("youtube-search");


const API_KEY = "AIzaSyDyrjfEFdX7OuyPj933N3oTF5DshDy2wNM";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.videoSearch=this.videoSearch.bind(this)
    this.state = {
      videos: [],
      selectedVideo:null
    };
    // Youtube Search

    this.videoSearch("AQUAMAN Trailer (2018)");
  }

  videoSearch(term){
    var opts = {
      maxResults: 10,
      key: API_KEY
    };
    
    search(term, opts, (err, results) => {
      if (err) return console.log(err);
      this.setState({
        videos: results,
        selectedVideo:results[6]
      });
      console.log("result:", results);
    });
  }

  
  render() {
    const VideoSearch=_.debounce((term)=>{this.videoSearch(term)},300);
    return (
      <div>
        <SearchBar onSearchTermChange={this.videoSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList videos={this.state.videos} onVideoSelect={selectedVideo => this.setState({selectedVideo}) }/>
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));
